package interfaces.view;

import interfaces.plugins.GamePlugin;
import interfaces.plugins.UIPlugin;

/**
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public interface GamePicker {
	
	/**
	 * This function is used to create a lobby where players (both people and computers) can join the game.
	 * Calls createGame on completion
	 * 
	 * @param gamePlugin the game plugin that was chosen by the user
	 * @param uiPlugin the ui plugin that was chosen by the user
	 */
	void createLobby(GamePlugin gamePlugin, UIPlugin uiPlugin, String gamePluginName);

}
