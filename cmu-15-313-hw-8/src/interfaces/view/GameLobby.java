package interfaces.view;
import interfaces.model.Player;
import interfaces.plugins.GamePlugin;
import interfaces.plugins.UIPlugin;

import java.util.List;

import javax.swing.*;


/**
 * The concrete class that will implement this interface will extend a JFrame.
 * It is in this view where the user will select a game and UI plugin
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public interface GameLobby{
	
	/**
	 * This function is used to generate a game given the game and ui plugins as well as a list of the players playing
	 * This class is responsible for creating a GameFrame object that will hold the UI for the game
	 * 
	 * @param gamePlugin the game plugin that was chosen by the user
	 * @param uiPlugin the ui plugin that was chosen by the user
	 * @param players the players playing the game
	 */
	void createGame(GamePlugin gamePlugin, UIPlugin uiPlugin, List<Player> players);
}
