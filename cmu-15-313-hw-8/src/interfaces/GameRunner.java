package interfaces;
import interfaces.plugins.GamePlugin;
import interfaces.plugins.UIPlugin;

import java.util.List;


/**
 * 
 */

/**
 * This class defines the way a game should be run.
 * It contains methods for acquiring UI and game plugins from the user, 
 * and dispatching a game based on what plugins the user has chosen.
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public interface GameRunner {
	/**
	 * Calls createPicker upon completion.
	 */
	void init();
	
	/**
	 * This function is used to generate the UI responsible for displaying all of the available
	 * game plugins and UI plugins for the user to choose from. When plugins are picked, this function
	 * will call createLobby on completion
	 * 
	 * @param gamePluginList a list of all of the game plugins available
	 * @param uiPluginList a list of all of the ui plugins available
	 */
	void createPicker(List<String> gamePluginNameList, List<String> uiPluginNameList);
	
	

}
