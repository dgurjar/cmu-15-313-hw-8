package interfaces.model;
import java.util.List;

/**
 * 
 */

/**
 * This class is a skeleton defining what a Player in a board game should be
 * able to do. The intended children of this class can either be a human player
 * or a computer player. This class is simply intended to hold information
 * about the player so that information can be easily accessed.
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public interface Player {
	
	/**
	 * The name of the specific player.
	 * This value should mostly be used to differentiate 2 players.
	 * It may also be used to intuitively display who's turn it is.
	 * @return the String name of this player.
	 */
	public String getName();

	
	public void setName(String name);

	
	/**
	 * The number of the team that this player is currently on.
	 * Depending on the specific game plugin this variable may change 
	 *   dynamically.
	 * A real instance of a player should always have this be a positive 
	 *   number.
	 * @return int representing the team this Player is on.
	 */
	public int getTeamNumber();
	
	/**
	 * Changes the number which defines which team this player is on.
	 * Thus changing which team the player is on.
	 * @param the new team number
	 */
	public void setTeamNumber(int newTeam);
	
}
