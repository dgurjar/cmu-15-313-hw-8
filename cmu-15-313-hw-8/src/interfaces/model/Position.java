package interfaces.model;

import java.awt.Point;

import interfaces.plugins.UIPlugin;
import interfaces.view.panels.InfoPanel;

public interface Position {

	final Point point = null;

	public Point getPoint();
	
	public String getTerrainType();
	
	public void setTerrainType(String type);
	
	public Piece getPiece();
	
	public void setPiece(Piece piece);

	public void setTerrainImage(UIPlugin uiPlugin);
	
	public void update(int positionsWidth, int positionsHeight);
	
}

