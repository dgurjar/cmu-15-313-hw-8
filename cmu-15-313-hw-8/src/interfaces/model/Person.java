package interfaces.model;
/**
 * 
 */

/**
 * This class represents a human player for the game.
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public interface Person extends Player {

}
