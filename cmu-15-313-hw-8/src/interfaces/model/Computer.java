package interfaces.model;


/**
 * This class represents a computer opponent for the game.
 * The plugin is responsible for implementing an AI for this opponent 
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public interface Computer extends Player {

	/**
	 * This represents the type of computer as defined by the plugin
	 * Examples include: easy, medium and hard types to denote difficulty.
	 * Usage of this variable is entirely optional
	 * @return the String representing what type of Computer this instance is
	 */
	public String type();
	
}
