package implementations;

import interfaces.plugins.GamePlugin;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.ResourceBundle;

public class GamePluginFactory {
	
	private static final String GAME_PLUGINS_CONFIGURATION = "gameplugins";
	public static Hashtable<String, String> gamePluginMappings = new Hashtable<String, String>();

	static {
		try {
			loadGamePluginMappings();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static GamePlugin getGamePlugin(String pluginName) {
        String className = gamePluginMappings.get(pluginName);
        GamePlugin gamePlugin = null;
 
        try {
            if( className!=null) {
                Class<?> cls = Class.forName(className);
                gamePlugin = (GamePlugin)cls.newInstance();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
 
        return gamePlugin;
    }
	
    private static void loadGamePluginMappings() {
        ResourceBundle rb = ResourceBundle.getBundle(GAME_PLUGINS_CONFIGURATION, Locale.getDefault());
        for (Enumeration<String> e = rb.getKeys(); e.hasMoreElements();) {
            String key = (String) e.nextElement();
            gamePluginMappings.put(key, rb.getString(key));
        }
    }

}
