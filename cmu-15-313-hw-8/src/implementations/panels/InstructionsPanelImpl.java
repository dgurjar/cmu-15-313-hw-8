package implementations.panels;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import interfaces.view.panels.InstructionsPanel;

public class InstructionsPanelImpl extends JPanel implements InstructionsPanel {

	private JLabel instructionsLabel = new JLabel();
	
	public InstructionsPanelImpl(String instructions){
		setLayout(new GridLayout(1,1));
		setBorder(BorderFactory.createTitledBorder("Instructions"));
		setPreferredSize(new Dimension(130,530));
		
		instructionsLabel.setText(instructions);
		instructionsLabel.setVerticalAlignment(JLabel.TOP);
		
		add(instructionsLabel);
	}
	
}
