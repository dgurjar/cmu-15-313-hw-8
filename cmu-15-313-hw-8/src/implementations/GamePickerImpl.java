package implementations;

import interfaces.plugins.GamePlugin;
import interfaces.plugins.UIPlugin;
import interfaces.view.GamePicker;

/**
 * 
 * @author bcharna
 *
 */
public class GamePickerImpl implements GamePicker {

	@Override
	public void createLobby(final GamePlugin gamePlugin, final UIPlugin uiPlugin, final String gamePluginName) {

		System.out.println("Lobby was just created.");
		System.out.println("Here are the plugins:");
		System.out.println(gamePlugin.getClass());
		System.out.println(uiPlugin.getClass());
		
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                GameLobby.createAndShowGUI(gamePlugin, uiPlugin, gamePluginName);
            }
        });

		
	}

}
