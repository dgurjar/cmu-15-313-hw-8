package implementations.tests;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import implementations.model.*;
import interfaces.model.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class BoardTests {
	
	/**
	 * Tests that a piece can be added to a square board
	 * Tests this by making sure that the added piece and the existing piece are
	 * 	controlled by the same person.
	 */
	@Test
	public void addPieceTestSq()
	{
		PositionImpl[][] board = new PositionImpl[3][3];
		Player james = new PlayerImpl("James", 0);
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
				board[i][j] = new PositionImpl(null, new Point(i,j), null);	
		}
		
		//Make the board through the constructor 
		SquareBoard myBoard = new SquareBoardImpl(board, 3,3);
		
		Position pos = myBoard.getPositionAt(new Point(1,1));
		Piece myPiece = new PieceImpl(james);
		
		pos.setPiece(myPiece);
		myBoard.addNewPosition(pos);

		boolean verification = false;
		if(myBoard.getPositionAt(new Point(1,1)).getPiece().getPlayer().getName().equals("James"))
			verification = true;
		
		assertTrue(verification);
	}
	
	
	/**
	 * Tests that a piece can be added to a hex board
	 * Tests this by making sure that the added piece and the existing piece are
	 * 	controlled by the same person.
	 */
	@Test
	public void addPieceTestHex()
	{
		PositionImpl[][] board = new PositionImpl[3][3];
		Player james = new PlayerImpl("James", 0);
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
				board[i][j] = new PositionImpl(null, new Point(i,j), null);	
		}
		
		//Make the board through the constructor 
		HexBoard myBoard = new HexBoardImpl(board, 3,3);
		
		Position pos = myBoard.getPositionAt(new Point(1,1));
		Piece myPiece = new PieceImpl(james);
		
		pos.setPiece(myPiece);
		myBoard.addNewPosition(pos);

		boolean verification = false;
		if(myBoard.getPositionAt(new Point(1,1)).getPiece().getPlayer().getName().equals("James"))
			verification = true;
		
		assertTrue(verification);
	}
	
	/**
	 * Sanity check to make sure that adding 2 pieces to the same square results
	 * 	in only 1 piece on the board.
	 */
	@Test
	public void onePiecePerPosition()
	{
		PositionImpl[][] board = new PositionImpl[3][3];
		Player james = new PlayerImpl("James", 0);
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
				board[i][j] = new PositionImpl(null, new Point(i,j), null);	
		}
		
		//Make the board through the constructor 
		SquareBoard myBoard = new SquareBoardImpl(board, 3,3);
		
		ArrayList<Player> list = new ArrayList<Player>();
		list.add(james);
		GameState state = new GameStateImpl(myBoard, list);
		
		Position pos = myBoard.getPositionAt(new Point(1,1));
		Piece myPiece = new PieceImpl(james);
		
		pos.setPiece(myPiece);
		myBoard.addNewPosition(pos);

		
		
		/*"Add" a second piece on top of the first*/
		pos = myBoard.getPositionAt(new Point(1,1));
		myPiece = new PieceImpl(james);
		
		pos.setPiece(myPiece);
		myBoard.addNewPosition(pos);
		
		boolean verification = false;
		if(state.getPositionsForPlayer("James").size() == 1)
			verification = true;
		
		assertTrue(verification);		
	}
	
	

	/**
	 * Given a point that has max possible adjacent points on a square board, makes sure
	 * that all of these points are accounted for
	 */
	@Test
	public void maxAdjacentTestSquare(){
		PositionImpl[][] board = new PositionImpl[3][3];
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
				board[i][j] = new PositionImpl(null, new Point(i,j), null);	
		}
		
		//Make the board through the constructor 
		SquareBoard myBoard = new SquareBoardImpl(board, 3,3);
		List<Point> adj = myBoard.getAdjacent(new Point(1,1));
		
		assertTrue(adj.size() == 8);
		assertTrue(listHas(adj, new Point(0,0)));
		assertTrue(listHas(adj, new Point(0,1)));
		assertTrue(listHas(adj, new Point(0,2)));
		assertTrue(listHas(adj, new Point(1,0)));
		assertTrue(listHas(adj, new Point(1,2)));
		assertTrue(listHas(adj, new Point(2,0)));
		assertTrue(listHas(adj, new Point(2,1)));
		assertTrue(listHas(adj, new Point(2,2)));
	}

	/**
	 * Given a point that has max possible adjacent points on a hex board, makes sure
	 * that all of these points are accounted for
	 */
	@Test
	public void maxAdjacentTestHex(){
		PositionImpl[][] board = new PositionImpl[5][5];
		for(int i=0;i<5;i++)
		{
			for(int j=0;j<5;j++)
				board[i][j] = new PositionImpl(null, new Point(i,j), null);	
		}
		
		//Make the board through the constructor 
		HexBoard myBoard = new HexBoardImpl(board, 5,5);
		List<Point> adj = myBoard.getAdjacent(new Point(2,1));
		
		
		assertTrue(adj.size() == 6);
		assertTrue(listHas(adj, new Point(0,1)));
		assertTrue(listHas(adj, new Point(1,0)));
		assertTrue(listHas(adj, new Point(1,2)));
		assertTrue(listHas(adj, new Point(3,0)));
		assertTrue(listHas(adj, new Point(3,2)));
		assertTrue(listHas(adj, new Point(4,1)));
	}
	
	/**
	 * Given a point that is on the very edge of a square board, 
	 * ensures that points outside of the board are not accounted for
	 */
	@Test
	public void edgeAdjacentTestSquare(){
		PositionImpl[][] board = new PositionImpl[3][3];
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
				board[i][j] = new PositionImpl(null, new Point(i,j), null);	
		}
		
		//Make the board through the constructor 
		SquareBoard myBoard = new SquareBoardImpl(board, 3,3);
		List<Point> adj = myBoard.getAdjacent(new Point(0,0));
		
		assertTrue(adj.size() == 3);
		assertTrue(listHas(adj, new Point(0,1)));
		assertTrue(listHas(adj, new Point(1,1)));
		assertTrue(listHas(adj, new Point(1,1)));
	}
	
	/**
	 * Given a point that is on the very edge of a hex board, 
	 * ensures that points outside of the board are not accounted for
	 */
	@Test
	public void edgeAdjacentTestHex(){
		PositionImpl[][] board = new PositionImpl[5][5];
		for(int i=0;i<5;i++)
		{
			for(int j=0;j<5;j++)
				board[i][j] = new PositionImpl(null, new Point(i,j), null);	
		}
		
		//Make the board through the constructor 
		HexBoard myBoard = new HexBoardImpl(board, 5,5);
		List<Point> adj = myBoard.getAdjacent(new Point(1,0));
		
		assertTrue(adj.size() == 3);
		
		assertTrue(listHas(adj, new Point(0,1)));
		assertTrue(listHas(adj, new Point(2,1)));
		assertTrue(listHas(adj, new Point(3,0)));
	}
	
	/**
	 * Given a point that is not on a square board, 
	 * ensures that no points are accounted for
	 */
	@Test
	public void invalidInputAdjacentTestSquare(){
		PositionImpl[][] board = new PositionImpl[3][3];
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
				board[i][j] = new PositionImpl(null, new Point(i,j), null);	
		}
		
		//Make the board through the constructor 
		SquareBoard myBoard = new SquareBoardImpl(board, 3,3);
		List<Point> adj = myBoard.getAdjacent(new Point(-1,0));
		assertTrue(adj == null);
		adj = myBoard.getAdjacent(new Point(0,-1));
		assertTrue(adj == null);
		adj = myBoard.getAdjacent(new Point(4,-2));
		assertTrue(adj == null);
		adj = myBoard.getAdjacent(new Point(2,3));
		assertTrue(adj == null);
	}
	
	/**
	 * Given a point that is not on a hex board, 
	 * ensures that no points are accounted for
	 */
	@Test
	public void invalidInputAdjacentTestHex(){
		PositionImpl[][] board = new PositionImpl[5][5];
		for(int i=0;i<5;i++)
		{
			for(int j=0;j<5;j++)
				board[i][j] = new PositionImpl(null, new Point(i,j), null);	
		}
		
		//Make the board through the constructor 
		HexBoard myBoard = new HexBoardImpl(board, 5,5);
		List<Point> adj = myBoard.getAdjacent(new Point(6,0));
		assertTrue(adj == null);
		adj = myBoard.getAdjacent(new Point(0,7));
		assertTrue(adj == null);
		adj = myBoard.getAdjacent(new Point(0,0));
		assertTrue(adj == null);
		adj = myBoard.getAdjacent(new Point(2,2));
		assertTrue(adj == null);
		adj = myBoard.getAdjacent(new Point(1,1));
		assertTrue(adj == null);
		adj = myBoard.getAdjacent(new Point(5,1));
		assertTrue(adj == null);
	}
	
	
	/*
	 * Notes about other tests to write:
	 * 	-	test that the adjacent square function is correct
	 * 	-
	 * Notes about other test classes to make:
	 * 	-	Player and Piece are in a sense tuples, so there isn't anything to
	 * 			test there.
	 * 	- 	The same can be said for Position, however the function onClick
	 * 			which may be moved should be tested when implemented.
	 * 	-	A test suite for GameState should be created.
	 * 
	 */
	
	
	
	/*
	 * While writing tests for the board, I noticed that the list contains 
	 * 	method was not acting as expected and thus decided that the compareTo
	 * 	function for Points was not what I thought. I am thus writting an 
	 * 	equivelant.
	 */
	private boolean listHas(List<Point> list, Point point)
	{
		for(Point pt : list)
		{
			if(pt.x == point.x && pt.y == point.y)
				return true;
		}
		return false;
	}
}
