package implementations.tests;

import static org.junit.Assert.*;
import implementations.model.*;
import interfaces.model.*;


import java.awt.List;
import java.awt.Point;
import java.util.ArrayList;

import org.junit.Test;

public class GameStateTests {

	@Test
	public void checkPlayerPositions() {
		PositionImpl[][] board = new PositionImpl[3][3];
		Player james = new PlayerImpl("James", 0);
		Player alex = new PlayerImpl("Alex", 1);
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
				board[i][j] = new PositionImpl(null, new Point(i,j), null);	
		}
		
		//Make the board through the constructor 
		HexBoard myBoard = new HexBoardImpl(board, 3,3);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(james);
		players.add(alex);
		GameState state = new GameStateImpl(myBoard, players);
		
		
		/*Add 1 new piece*/
		Position pos = myBoard.getPositionAt(new Point(1,1));
		Piece myPiece = new PieceImpl(james);
		
		pos.setPiece(myPiece);
		myBoard.addNewPosition(pos);

		/*Add second new piece*/
		pos = myBoard.getPositionAt(new Point(2,1));
		myPiece = new PieceImpl(james);
		
		pos.setPiece(myPiece);
		myBoard.addNewPosition(pos);

		/*Add second player piece*/
		pos = myBoard.getPositionAt(new Point(0,0));
		myPiece = new PieceImpl(alex);
		
		pos.setPiece(myPiece);
		myBoard.addNewPosition(pos);
		
		/*Test it*/
		ArrayList<Position> pieces = (ArrayList<Position>) state.getPositionsForPlayer("James"); 
		assertTrue(pieces.size() == 2);
	}

	@Test
	public void checkTeamPositions() {
		PositionImpl[][] board = new PositionImpl[3][3];
		Player james = new PlayerImpl("James", 0);
		Player alex = new PlayerImpl("Alex", 0);
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
				board[i][j] = new PositionImpl(null, new Point(i,j), null);	
		}
		
		//Make the board through the constructor 
		HexBoard myBoard = new HexBoardImpl(board, 3,3);
		ArrayList<Player> players = new ArrayList<Player>();
		players.add(james);
		players.add(alex);
		GameState state = new GameStateImpl(myBoard, players);
		
		
		/*Add 1 new piece*/
		Position pos = myBoard.getPositionAt(new Point(1,1));
		Piece myPiece = new PieceImpl(james);
		
		pos.setPiece(myPiece);
		myBoard.addNewPosition(pos);

		/*Add second new piece*/
		pos = myBoard.getPositionAt(new Point(2,1));
		myPiece = new PieceImpl(james);
		
		pos.setPiece(myPiece);
		myBoard.addNewPosition(pos);

		/*Add second player piece*/
		pos = myBoard.getPositionAt(new Point(0,0));
		myPiece = new PieceImpl(alex);
		
		pos.setPiece(myPiece);
		myBoard.addNewPosition(pos);
		
		
		/*Test it*/
		ArrayList<Position> pieces = (ArrayList<Position>) state.getPositionsForTeam(0); 
		assertTrue(pieces.size() == 3);
		assertTrue(state.getPositionsForPlayer("James").size() == 2);
		assertTrue(state.getPositionsForPlayer("Alex").size() == 1);
	}
	
}
