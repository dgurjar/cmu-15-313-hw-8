package implementations.model;
import interfaces.model.*;


/**
 * This class is a skeleton defining what a Player in a board game should be
 * able to do. The intended children of this class can either be a human player
 * or a computer player. This class is simply intended to hold information
 * about the player so that information can be easily accessed.
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public class PlayerImpl implements Player {	
	private String name;
	private int team;
	
	public PlayerImpl(String name, int team)
	{
		this.name = name;
		this.team = team;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getTeamNumber() {
		return team;
	}
	
	@Override
	public void setTeamNumber(int newTeam){
		team = newTeam;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

}
