package implementations.model;

import interfaces.model.Computer;
import interfaces.model.Player;

public class ComputerImpl extends PlayerImpl implements Computer{
	private String type;

	public ComputerImpl(String name, int team, String type) {
		super(name, team);
		this.type = type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String type() {
		return type;
	}

}
