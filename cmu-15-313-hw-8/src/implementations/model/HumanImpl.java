package implementations.model;

import interfaces.model.Player;

public class HumanImpl extends PlayerImpl implements Player{

	public HumanImpl(String name, int team) {
		super(name, team);
	}

}
